# tuxedo-scripts

## Purpose

This is just a few small tools that I'm using to improve how I use my Tuxedo Stellaris 15 laptop.  Currently, it's a tool that makes the numlock key's LED light up green when numlock is on and red when numlock is off.  I need this because there's no dedicated LED on the system for numlock, and I use the number pad in both modes quite often.

## Files

### quiet

This is just a simple mnemonic I throw into my system PATH.  If I want to run a command from a bash shell but be able to close tht bash shell afterwards without killing the command, I run this before the command, like so:

```
quiet command arg1 arg2 arg3
```

### set_kled

IMPORTANT:  This command must be able to run via sudo without a password.  It's a script, so we can't setuid it.

Syntax:

    set_kled <ledcode> <r g b>

Example:

    set_kled 99 "0 0 255"

This sets keyboard LED code 99 (numlock on this Stellaris) to blue

Example:

    set_kled all "255 0 0"

This sets every keyboard LED to red.  Note:  This greedily iterates through all LEDs, but there's a quicker way I need to test out.

### numlight

In general, this periodically monitors the state of one of the lock keys (numlock, capslock, etc) and sets the LED of the key (or any key you want) to a color based on that state.

NOTE:  numlight and set_kled must be in the same directory, though you could modify the numlight script to give set_kled a specific path.

Syntax:

    numlight -n <key name> -f <key field> -c <LED code> <on color> <off color>

Example:

    numlight -n "Num Lock" -f 8 -c 99 "0 255 0" "255 0 0"

This checks the "xset q" command every 0.2 seconds (the default) for the string "Num Lock" and extracts the eighth field, which is the numlock state in that line.  It then checks the current state of the led againnst this value and, if necessary, sets it to "0 255 0" (green) for "on" or "255 0 0" (red) for "off".

Example:

    numlight -p num

This uses numlight's "num" preset to do the same thing

## Questions

### Why not run one script as root?

You need to be the regular user in order to detect the key state, otherwise it complains about X stuff.
